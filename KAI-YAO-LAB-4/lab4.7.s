.data
msg1: .asciiz "please enter two non-negative integers: "
msg2: .asciiz "the Arckermann function result is: "
msg3: .asciiz "Wrong value: negative number entered,please try again!\n"

.text
.globl main

main:
    li $v0, 4               # system call for print_str
    la $a0, msg1            # print the msg1
    syscall
    li $v0, 5               # system call for read_int
    syscall
    move $t0, $v0           # store the integer into $t0
    li $v0, 5               # system call for read_int
    syscall
    move $t1, $v0           # store the integer into $t1
    bltz $t0, exception     # if the $t0 < 0,goto exception
    bltz $t1, exception     # if the $t1 < 0,goto exception
    j work                  # goto work if both are non integers



work:
    li $v0, 4               # system call for print_str
    la $a0, msg2            # print msg2
    syscall
    addi $sp, $sp, -4       # reserver stack space
    sw $ra, 4($sp)          # push $ra into the stack
    move $a0, $t0           # $a0 <- $t0
    move $a1, $t1           # $a1 <- $t1
    jal Ackermann           # call ackermann function
    move $a0, $v0           # $a0 <- $v0
    li $v0, 1               # system call for print_int
    syscall                 # print the result
    lw $ra, 4($sp)          # pop $ra
    addi $sp, $sp, 4        # restore sp
    jr $ra                  # return to main

Ackermann:
    beq $a0, $0, x0         # if $a0 ==$0,goto x0
    addi $sp, $sp, -4       # reserver stack space
    sw $ra, 4($sp)          # push $ra into the stack
    beq $a1, $0, y0         # if $a1 ==$0,goto y0
    addi $t0, $a0, -1       # $t0 <- $a0 + (-1)
    addi $sp, $sp, -4       # reserver stack space
    sw $t0, 4($sp)          # save（x - 1） in stack
    addi $a1, $a1, -1       # $a1 <- $a1 + (-1)
    jal Ackermann           # A(x, y - 1)
    lw $a0, 4($sp)          # pop $a0
    addi $sp, $sp, 4        # restore sp
    move $a1, $v0           # $a1 <- $v0
    jal Ackermann           # A(x - 1, A(x, y - 1))
    j end

x0:                         # x == 0
    addi $v0, $a1, 1        # $v0 <- $a1 + 1
    jr $ra                  # return to main

y0:                         # y == 0
    addi $a0, $a0, -1       # $a0 <- $a0 + (-1)
    li $a1, 1               # system call for print_int
    jal Ackermann           # A(x - 1, 1)

exception:
    li $v0, 4               # system call for print_str
    la $a0, msg3            # print error message
    syscall
    j main                  # goto main and try again
end:
    lw $ra, 4($sp)         # pop $ra
    addi $sp, $sp, 4       # restore $sp
    jr $ra                 # return to main
