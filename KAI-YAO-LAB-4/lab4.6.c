#include<iostream>
using namespace std;

//Ackerman function
int Ackerman(int x, int y);

//man function
void main()
{
    int x, y;
    cout << "pls input x、y：";
    cin >> x >> y;
    cout << "Ackerman function result is：";
    cout << Ackerman(x, y) << endl;
}

//Ackerman function
int Ackerman(int x, int y)
{
    if (0 == x)
        return y + 1;//if x=0，return y+1
    else
    {
        if (y > 0 && 0 == y)
            return Ackerman(x - 1, 1);//if x>0&y=0，return Ackerman（x-1，1）
        else
            if (x > 0 && y > 0)
                return Ackerman(x - 1,Ackerman(x, y - 1));//if x>0&y>0，return Ackerman(x - 1,Ackerman(x, y - 1))
    }
}
