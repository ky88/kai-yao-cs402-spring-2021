.data 0x10010000
var1: .word 0x0036    #var1 is a word (32 bit) with the initial value 36
var2: .word 0x0004
.extern ext1 4		# ext1 is a word (32 bit) in the extern space
.extern	ext2 4		# ext2 is a word (32 bit) in the extern space

.text
.globl main

main:
addu $s0, $ra,$0   # save $31 in $16
li $t0, 1
move $t1, $t0

lw $t1, var1
lw $t2, var2

la $t0, ext1
sw $t1, 0($t0)
la $t0, ext2
sw $t2, 4($t0)

# restore now the return address in $ra and return from main
addu $ra, $0, $s0    # return address back in $31
jr $ra               # return from main
