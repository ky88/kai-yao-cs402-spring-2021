.data 0x10010000
var1: .word 0x0083     #var1 is a word (32 bit) with the initial value 83
var2: .word 0x0104
var3: .word 0x0111
var4: .word 0x0119
first: .byte 'k'
last:  .byte 'y'

.text
.globl main

main:

addu $s0, $ra,$0   # save $31 in $16
ori $t0, $0, 1     
addu $t1, $0, $t0  
#swap value of var1 and var4 
lui $t2, 4097 [var1]  # load var1 address
lui $at, 4097 [var4]  # load var4 address

# temparily store var4 value in register $t4
ori $t3, $at, 12 [var4]
lui $at, 4097      
lw $t4, 12($1)  

# temparily store var1 value in register $t5   
lui $at, 4097      
lw $t5, 0($1)
     
sw $t4, 0($11)     # swap var4 into var1
sw $t5, 0($10)     # swap var1 into var4

  
lui $at, 4097 [var2]  # load var2 address
ori $t2, $at, 4 [var2]  
lui $at, 4097 [var3]  # load var3 address

# swap var2 and var3
ori $t3, $1, 8 [var3]  
lui $at, 4097      
lw $t4, 4($1)      
lui $at, 4097      
lw $t5, 8($1)      
sw $t4, 0($11)     
sw $t5, 0($10)  
   
# restore now the return address in $ra and return from main
addu $ra, $0, $s0    # return address back in $31
jr $ra               # return from main
