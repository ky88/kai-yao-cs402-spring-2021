.data 0x10010000
var1: .word 0x0083     #var1 is a word (32 bit) with the initial value 83
var2: .word 0x0104
var3: .word 0x0111
var4: .word 0x0119
first: .byte 'k'
last:  .byte 'y'

.text
.globl main

main: addu $s0, $ra,$0   # save $31 in $16
li $t0, 1
move $t1, $t0
#swap value of var1 and var4
la $t2, var1   # load var1 address
la $t3, var4   # load var4 address

lw $t4, var4   # temparily store var4 value in register $t4
lw $t5, var1   # temparily store var1 value in register $t5

sw $t4 0($t3)  # swap var4 into var1
sw $t5 0($t2)  # swap var1 into var4
#swap value of var2 and var3
la $t2, var2   # load var2 address
la $t3, var3   # load var3 address

lw $t4, var2   # temparily store var2 value in register $t4
lw $t5, var3   # temparily store var3 value in register $t5

sw $t4 0($t3)  # swap var3 into var2
sw $t5 0($t2)  # swap var2 into var3



# restore now the return address in $ra and return from main
addu $ra, $0, $s0    # return address back in $31
jr $ra               # return from main
