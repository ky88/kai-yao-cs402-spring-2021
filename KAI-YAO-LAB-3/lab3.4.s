.data
my_array:  .space 40   # reserver 40 bytes for ten words
initial_value: .word 3

.text
.globl main

main:

lw $t1 ,initial_value($0)   # $t1(j) <- initial_value
la $t0, my_array            # $t0 <- my_array start address
addi $a1, $t0, 40         # $a1 <- my_array end address

loop:
ble $a1, $t0 exit           # if $t0 >= $a1 ,exit loop
sw $t1, 0($t0)              # my_array[i]=j
addi $t1, $t1, 1            # j++
addi $t0, $t0, 4            # get next element address
j loop

exit:
jr $ra
