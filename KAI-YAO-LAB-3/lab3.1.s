.data
var1: .word 3
var2: .word 6
var3: .word -2021

.text
.globl main

main:
lw $t0, var1($0)   # $t0 <- var1
lw $t1, var2($0)   # $t1 <- var2
bne $t0, $t1, else  # if $t0!= $t1 go to label "else"

lw $t2, var3($0)  # $t2 <- var3
sw $t2, var1($0)  # var1 = var3
sw $t2, var2($0)  # var2 = var3
beq $0,$0, final  # go to label "final"

# swap var1 and var2
else:
move $t2, $t0
move $t0, $t1
move $t1, $t2
sw $t0, var1($0)
sw $t1, var2($0)

final:

li $v0, 1  # printint
lw $a0, var1($0)
syscall
li $v0,1
lw $a0, var2($0)
syscall
jr $ra
